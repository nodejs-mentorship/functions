class Car {
  constructor(make, model, type, cubicCapacity, mileage) {
    this.make = make;
    this.model = model;
    this.type = type;
    this.cubicCapacity = cubicCapacity;
    this.mileage = mileage;
  }

  setMileage(mileage) {
    this.mileage = mileage;
  }

  printCarDetails() {
    console.log(this.make, this.model);
    console.log("Type: ", this.type);
    console.log("Cubic capacity: ", this.cubicCapacity, "cc");
    console.log("Current mileage: ", this.mileage, "km");
  }
}

const car = new Car("BMW", "X5", "SUV", 2000, 75000);
car.printCarDetails();
car.setMileage(88006);
car.printCarDetails();

console.log();

const zoo = {
  animals: [
    { name: "Tiger", count: 5 },
    { name: "Lion", count: 2 },
    { name: "Elephant", count: 1 },
    { name: "Monkey", count: 8 },
  ],
  printListOfAnimals() {
    console.log("Animals in zoo: ");
    this.animals.forEach((animal) => {
      const printName = animal.count <= 1 ? animal.name : `${animal.name}s`;
      console.log(`${animal.count}x ${printName}`);
    });
  },
  addAnimal(name, count) {
    const animalIdx = this.animals.findIndex(
      (a) => a.name.toLowerCase() === name.toLowerCase()
    );
    if (animalIdx > 0) {
      this.animals[animalIdx].count += count;
    } else {
      this.animals.push({ name, count });
    }
  },
};

zoo.printListOfAnimals();
zoo.addAnimal("Cat", 2);
zoo.addAnimal("Lion", 4);
zoo.printListOfAnimals();
